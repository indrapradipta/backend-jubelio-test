const { init, start } = require('./lib');
const startServer = async () => {
  await init();
  await start();
};

startServer();
