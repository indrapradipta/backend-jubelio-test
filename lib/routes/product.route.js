const Joi = require('joi');
const productHandler = require('../handlers/products.handlers');

module.exports = [
  {
    method: 'GET',
    path: '/import-products',
    options: { auth: 'secretkey' },
    handler: productHandler.importEleveniaProducts,
  },
  {
    method: 'GET',
    path: '/products',
    options: { auth: 'secretkey' },
    handler: productHandler.productList,
  },
  {
    method: 'GET',
    path: '/product/{product_id}',
    options: { auth: 'secretkey' },
    handler: productHandler.productDetail,
  },
  {
    method: 'POST',
    path: '/product',
    handler: productHandler.createProduct,
    options: {
      auth: 'secretkey',
      validate: {
        payload: Joi.object({
          name: Joi.string().required(),
          sku: Joi.string().required(),
          image: Joi.string().required(),
          price: Joi.number().min(1000).required(),
          description: Joi.string().optional(),
        }),
        failAction(request, h, err) {
          throw err;
        },
        options: {
          abortEarly: false,
        },
      },
    },
  },
  {
    method: 'PUT',
    path: '/product/{product_id}',
    handler: productHandler.updateProduct,
    options: {
      auth: 'secretkey',
      validate: {
        payload: Joi.object({
          name: Joi.string().required(),
          sku: Joi.string().required(),
          image: Joi.string().required(),
          price: Joi.number().min(1000).required(),
          description: Joi.string().optional(),
        }),
        failAction(request, h, err) {
          throw err;
        },
        options: {
          abortEarly: false,
        },
      },
    },
  },
  {
    method: 'DELETE',
    path: '/product/{product_id}',
    handler: productHandler.deleteProduct,
    options: {
      auth: 'secretkey',
      validate: {
        params: Joi.object({
          product_id: Joi.string().required(),
        }),
        failAction(request, h, err) {
          throw err;
        },
        options: {
          abortEarly: false,
        },
      },
    },
  },
];
