const axios = require('axios');
const convert = require('xml-js');

const getEleveniaProducts = async (totalPage) => {
  const url =
    'http://api.elevenia.co.id/rest/prodservices/product/listing?page=';

  const productIds = [];
  for (let i = 1; i < totalPage + 1; i++) {
    const res = await axios.get(`${url}${i}`, {
      headers: {
        openapikey: process.env.ELEVENIA_OPEN_API,
      },
    });

    const pIds = convert
      .xml2js(res.data, { compact: true, spaces: 4 })
      .Products.product.map((x) => ({
        id: x.prdNo._text,
      }));

    productIds.push(...pIds);
  }

  const data = [];
  for (let i = 0; i < productIds.length; i++) {
    const productId = productIds[i].id;
    const productDetail = await getProductDetail(productId);
    const productStock = await getProductStock(productId);

    const product = {
      id: productDetail.id,
      name: productDetail.name,
      image: productDetail.image,
      description: productDetail.description,
      price: productDetail.price,
      sku: productStock.sku,
    };
    data.push(product);
  }

  return data;
};

const getProductDetail = async (productId) => {
  const url = `http://api.elevenia.co.id/rest/prodservices/product/details/${productId}`;
  const res = await axios.get(url, {
    headers: {
      openapikey: process.env.ELEVENIA_OPEN_API,
    },
  });
  const data = convert.xml2js(res.data, { compact: true, spaces: 4 }).Product;
  return {
    id: data.prdNo?._text,
    name: data.prdNm?._text,
    price: data.selPrc?._text,
    description: data.htmlDetail?._text,
    image: data.prdImage01?._text || '',
  };
};

const getProductStock = async (productId) => {
  const url = `http://api.elevenia.co.id/rest/prodmarketservice/prodmarket/stck/${productId}`;
  const res = await axios.get(url, {
    headers: {
      openapikey: process.env.ELEVENIA_OPEN_API,
    },
  });
  const data = convert.xml2js(res.data, { compact: true, spaces: 4 });

  return { sku: data.ProductStocks.sellerPrdCd?._text };
};

module.exports = {
  getEleveniaProducts,
};
