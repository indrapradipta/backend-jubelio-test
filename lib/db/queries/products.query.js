const Boom = require('@hapi/boom');
const { v4: uuidv4 } = require('uuid');
const db = require('../config');

const listProduct = async (args) => {
  const page = Number(args.page) || 1;
  const limit = Number(args.limit) || 10;
  const sort_key = args.sort_key || 'created_at';
  const sort_value = args.sort_value || 'ASC';
  const offset = (page - 1) * limit;

  const query = 'SELECT * FROM products';
  const queryWithLimit =
    query +
    ` ORDER BY ${sort_key} ${sort_value} LIMIT ${limit} OFFSET ${offset}`;

  let data = [];
  let totalData = 0;
  try {
    data = await db.many(queryWithLimit);
    const allData = await db.many(query);
    totalData = allData.length;
  } catch (e) {
    console.log(e);
  }
  const pages = Math.ceil(totalData / limit);

  return {
    total: totalData,
    page,
    pages,
    limit,
    data: data.map((x) => ({
      id: x.id,
      name: x.name,
      image: x.image,
      sku: x.sku,
      price: x.price,
    })),
  };
};

const bulkCreateProduct = async (productData) => {
  const result = [];
  for (let i = 0; i < productData.length; i++) {
    const { name, sku, image, price, description } = productData[i];
    const uuid = uuidv4();

    // Check product SKU is exist in database
    let check = null;
    try {
      check = await db.one(`SELECT * FROM products WHERE sku ILIKE $1`, [sku]);
    } catch (e) {}

    if (check) {
      continue;
    }

    //Insert data to Database
    let dt = null;
    try {
      dt = await db.one(
        `INSERT INTO products(id,name,sku,image,price,description) VALUES ($1,$2,$3,$4,$5,$6) RETURNING *`,
        [uuid, name, sku, image, price, description]
      );
    } catch (e) {
      console.log(e);
      throw Boom.internal('Failed to create product');
    }

    result.push(dt);
  }

  return result;
};

const findProduct = async (productId) => {
  //Insert data to Database
  let result = null;
  try {
    result = await db.one(`SELECT * FROM products WHERE id='${productId}'`);
  } catch (e) {
    const x = { ...e };
    if (x.received === 0) throw Boom.notFound();
    console.log(e);
    throw Boom.internal();
  }

  return result;
};

const createProduct = async ({ name, sku, image, price, description }) => {
  const uuid = uuidv4();
  // Check product SKU is exist in database
  let check = null;
  try {
    check = await db.one(`SELECT * FROM products WHERE sku ILIKE $1`, [sku]);
  } catch (e) {}

  if (check) {
    throw Boom.badRequest('SKU already exist');
  }

  //Insert data to Database
  let result = null;
  try {
    result = await db.one(
      `INSERT INTO products(id,name,sku,image,price,description) VALUES ($1,$2,$3,$4,$5,$6) RETURNING *`,
      [uuid, name, sku, image, price, description]
    );
  } catch (e) {
    console.log(e);
    throw Boom.internal('Failed to create product');
  }

  return result;
};

const updateProduct = async (
  productId,
  { name, sku, image, price, description }
) => {
  // Check product SKU is exist in database
  let check = null;
  try {
    check = await db.one(
      `SELECT * FROM products WHERE sku ILIKE $1 AND NOT id=$2`,
      [sku, productId]
    );
  } catch (e) {}

  if (check) {
    throw Boom.badRequest('SKU already exist');
  }

  //Insert data to Database
  let result = null;
  try {
    result = await db.one(
      `UPDATE products SET name = $1, sku = $2, image = $3, price = $4, description = $5 WHERE id = $6 RETURNING *`,
      [name, sku, image, price, description, productId]
    );
  } catch (e) {
    console.log(e);
    throw Boom.internal('Failed to update product');
  }

  return result;
};

const deleteProduct = async (productId) => {
  // Check product if it exits
  let check = null;
  try {
    check = await db.one(`SELECT * FROM products WHERE id = '${productId}'`);
  } catch (e) {}

  if (!check) {
    throw Boom.notFound('Product not found');
  }

  //Delete data from Database
  try {
    result = await db.result(`DELETE FROM products WHERE id = '${productId}'`);
  } catch (e) {
    console.log(e);
    throw Boom.internal('Failed to delete product');
  }

  return { id: productId };
};

module.exports = {
  listProduct,
  createProduct,
  bulkCreateProduct,
  findProduct,
  updateProduct,
  deleteProduct,
};
