const db = require('../config');
const argv = require('minimist')(process.argv.slice(2));

// Run Migration
const runMigration = async () => {
  // Create products table query
  const query = `CREATE TABLE IF NOT EXISTS products ( id uuid NOT NULL, name VARCHAR (255) NOT NULL, sku VARCHAR (255) UNIQUE NOT NULL, image VARCHAR (255) NOT NULL, price INT NOT NULL, description TEXT, created_at TIMESTAMP NOT NULL DEFAULT now(), updated_at TIMESTAMP NOT NULL DEFAULT now(), deleted_at TIMESTAMP)`;

  console.log('Run Migration');
  console.log('Run Query : ', query);
  await db.query(query);
  console.log('Migration Success');
};

// Revert Migration
const revertMigration = async () => {
  // Drop table products query
  const query = 'DROP TABLE IF EXISTS products';

  console.log('Revert Migration');
  console.log('Run Query : ', query);
  await db.query(query);
  console.log('Migration Reverted');
};

const migration = async () => {
  switch (argv.m) {
    case 'run':
      await runMigration();
      break;
    case 'revert':
      await revertMigration();
      break;
    default:
      console.log('Please add option "-m [run, revert]"');
  }
  process.exit();
};

migration();
