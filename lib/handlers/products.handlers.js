const eleveniaApi = require('../party/elevenia.api');
const productQueries = require('../db/queries/products.query');
const Boom = require('@hapi/boom');

const importEleveniaProducts = async (request, h) => {
  const totalPage = Number(request.query.total_page) || 2;
  let data = null;
  try {
    data = await eleveniaApi.getEleveniaProducts(totalPage);
  } catch (e) {
    console.log(e);
    throw Boom.internal();
  }

  const result = await productQueries.bulkCreateProduct(data);
  return {
    success: true,
    statusCode: 200,
    message: `${result.length} product data has been imported`,
    result,
  };
};

const productList = async (request, h) => {
  const result = await productQueries.listProduct(request.query);
  return {
    success: true,
    statusCode: 200,
    message: 'Success',
    result,
  };
};

const productDetail = async (request, h) => {
  const result = await productQueries.findProduct(request.params.product_id);
  return {
    success: true,
    statusCode: 200,
    message: 'Success',
    result,
  };
};

const createProduct = async (request, h) => {
  const result = await productQueries.createProduct(request.payload);
  return {
    success: true,
    statusCode: 200,
    message: 'Product has been created',
    result,
  };
};

const updateProduct = async (request, h) => {
  const result = await productQueries.updateProduct(
    request.params.product_id,
    request.payload
  );
  return {
    success: true,
    statusCode: 200,
    message: 'Product has been updated',
    result,
  };
};

const deleteProduct = async (request, h) => {
  const result = await productQueries.deleteProduct(request.params.product_id);
  return {
    success: true,
    statusCode: 200,
    message: 'Product has been deleted',
    result,
  };
};

module.exports = {
  importEleveniaProducts,
  productList,
  productDetail,
  createProduct,
  updateProduct,
  deleteProduct,
};
