'use strict';

const Hapi = require('@hapi/hapi');
const mainRoute = require('./routes/main.route');
const productRoute = require('./routes/product.route');
const { scheme } = require('./utils/validate-secretkey');
require('dotenv').config();

const server = Hapi.server({
  port: process.env.PORT,
  host: 'localhost',
  routes: {
    cors: true,
  },
});

server.auth.scheme('custom', scheme);
server.auth.strategy('secretkey', 'custom');

server.route([...mainRoute, ...productRoute]);

exports.init = async () => {
  await server.initialize();
  return server;
};

exports.start = async () => {
  await server.start();
  server
    .table()
    .forEach((route) => console.log(`${route.method}\t${route.path}`));
  server.events.on('response', function (request) {
    console.log(
      request.info.remoteAddress +
        ': ' +
        request.method.toUpperCase() +
        ' ' +
        request.path +
        ' --> ' +
        request.response.statusCode
    );
  });
  console.log(`Server running at: ${server.info.uri}`);
  return server;
};
