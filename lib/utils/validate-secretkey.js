const Boom = require('@hapi/boom');

require('dotenv').config();

exports.scheme = function (server, options) {
  return {
    authenticate: function (request, h) {
      const req = request.raw.req;
      const authorization = req.headers.authorization;
      if (authorization !== `Bearer ${process.env.API_SECRET_KEY}`)
        throw Boom.unauthorized();

      return h.authenticated({ credentials: { isValid: true } });
    },
  };
};
