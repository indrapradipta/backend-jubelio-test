'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it, before, after } = (exports.lab =
  Lab.script());
const { init } = require('../lib');
const db = require('../lib/db/config');
require('dotenv').config();

describe('Products routes', () => {
  let server;
  let products = [];

  before(async () => {
    await db.query('START TRANSACTION');
  });

  after(async () => {
    await db.query('ROLLBACK');
  });

  beforeEach(async () => {
    server = await init();
  });

  afterEach(async () => {
    await server.stop();
  });

  describe('GET /import-products', { timeout: 100000 }, () => {
    it('response with unauthorization', async () => {
      const res = await server.inject({
        method: 'get',
        url: `/import-products`,
      });
      expect(res.statusCode).to.be.equal(401);
    });

    it('response with products imported from elevenia', async () => {
      const res = await server.inject({
        method: 'get',
        url: `/import-products?total_page=1`,
        headers: {
          Authorization: 'Bearer ' + process.env.API_SECRET_KEY,
        },
      });
      expect(res.statusCode).to.equal(200);
    });
  });

  describe('POST /product', () => {
    const payload = {
      name: 'Product Name',
      sku: `Product SKU ${Date.now()}`,
      image: 'Product Image',
      description: 'Product Description',
      price: 10000,
    };

    it('response with unauthorization', async () => {
      const res = await server.inject({
        method: 'post',
        url: `/product`,
        payload,
      });
      expect(res.statusCode).to.be.equal(401);
    });

    it('response with bad request', async () => {
      const res = await server.inject({
        method: 'post',
        url: `/product`,
        payload: {},
        headers: {
          Authorization: 'Bearer ' + process.env.API_SECRET_KEY,
        },
      });

      expect(res.statusCode).to.be.equal(400);
    });

    it('response with payload product created', async () => {
      const res = await server.inject({
        method: 'post',
        url: `/product`,
        payload,
        headers: {
          Authorization: 'Bearer ' + process.env.API_SECRET_KEY,
        },
      });
      expect(res.statusCode).to.be.equal(200);
      const jsonResponse = JSON.parse(res.payload);
      expect(jsonResponse.result).to.be.an.object();
      expect(jsonResponse.result.id).to.be.string();
      expect(jsonResponse.result.name).to.equal(payload.name);
      expect(jsonResponse.result.sku).to.equal(payload.sku);
      expect(jsonResponse.result.image).to.equal(payload.image);
      expect(jsonResponse.result.price).to.equal(payload.price);
      expect(jsonResponse.result.description).to.equal(payload.description);
    });
  });

  describe('GET /products', () => {
    it('response with unauthorization', async () => {
      const res = await server.inject({
        method: 'get',
        url: '/products',
      });

      expect(res.statusCode).to.be.equal(401);
    });

    it('response with payload products', async () => {
      const res = await server.inject({
        method: 'get',
        url: '/products',
        headers: {
          Authorization: 'Bearer ' + process.env.API_SECRET_KEY,
        },
      });

      expect(res.statusCode).to.be.equal(200);
      const jsonResponse = JSON.parse(res.payload);
      products = jsonResponse.result.data;
      expect(jsonResponse.result.data).to.be.an.array();
    });

    it('response with payload products with queries', async () => {
      const page = 1;
      const limit = 1;
      const res = await server.inject({
        method: 'get',
        url: `/products?page=${page}&limit=${limit}`,
        headers: {
          Authorization: 'Bearer ' + process.env.API_SECRET_KEY,
        },
      });

      expect(res.statusCode).to.be.equal(200);
      const jsonResponse = JSON.parse(res.payload);
      const pages = Math.ceil(jsonResponse.result.total / limit);
      expect(jsonResponse.result.data).to.be.an.array();
      expect(jsonResponse.result.page).to.equal(page);
      expect(jsonResponse.result.limit).to.equal(limit);
      expect(jsonResponse.result.pages).to.equal(pages);
    });
  });

  describe('GET /product', () => {
    it('response with unauthorization', async () => {
      const pdt = products[0];
      const res = await server.inject({
        method: 'get',
        url: `/product/${pdt.id}`,
        headers: {
          Authorization: 'Bearer ' + process.env.API_SECRET_KEY,
        },
      });
      expect(res.statusCode).to.be.equal(200);
    });

    it('response with product not found', async () => {
      const res = await server.inject({
        method: 'get',
        url: `/product/3e550a41-22fb-4c9a-9319-7aaf770aa943`,
        headers: {
          Authorization: 'Bearer ' + process.env.API_SECRET_KEY,
        },
      });

      expect(res.statusCode).to.be.equal(404);
    });

    it('response with payload product detail', async () => {
      const pdt = products[0];
      const res = await server.inject({
        method: 'get',
        url: `/product/${pdt.id}`,
        headers: {
          Authorization: 'Bearer ' + process.env.API_SECRET_KEY,
        },
      });

      expect(res.statusCode).to.be.equal(200);
      const jsonResponse = JSON.parse(res.payload);
      expect(jsonResponse.result).to.be.an.object();
      expect(jsonResponse.result.id).to.equal(pdt.id);
      expect(jsonResponse.result.name).to.equal(pdt.name);
      expect(jsonResponse.result.sku).to.equal(pdt.sku);
      expect(jsonResponse.result.image).to.equal(pdt.image);
      expect(jsonResponse.result.price).to.equal(pdt.price);
      expect(jsonResponse.result.description).to.be.an.string();
    });
  });

  describe('PUT /product/{product_id}', () => {
    const payload = {
      name: 'Update Product Name',
      sku: `Update Product SKU ${Date.now()}`,
      image: 'Update Product Image',
      description: 'Update Product Description',
      price: 10000,
    };

    it('response with unauthorization', async () => {
      const pId = products[0].id;
      const res = await server.inject({
        method: 'put',
        url: `/product/${pId}`,
        payload,
      });
      expect(res.statusCode).to.be.equal(401);
    });

    it('response with payload bad request', async () => {
      const pId = products[0].id;
      const res = await server.inject({
        method: 'put',
        url: `/product/${pId}`,
        payload: {},
        headers: {
          Authorization: 'Bearer ' + process.env.API_SECRET_KEY,
        },
      });

      expect(res.statusCode).to.be.equal(400);
    });

    it('response with payload product created', async () => {
      const pId = products[0].id;
      const res = await server.inject({
        method: 'put',
        url: `/product/${pId}`,
        payload,
        headers: {
          Authorization: 'Bearer ' + process.env.API_SECRET_KEY,
        },
      });
      expect(res.statusCode).to.be.equal(200);
      const jsonResponse = JSON.parse(res.payload);
      expect(jsonResponse.result).to.be.an.object();
      expect(jsonResponse.result.id).to.equal(pId);
      expect(jsonResponse.result.name).to.equal(payload.name);
      expect(jsonResponse.result.sku).to.equal(payload.sku);
      expect(jsonResponse.result.image).to.equal(payload.image);
      expect(jsonResponse.result.price).to.equal(payload.price);
      expect(jsonResponse.result.description).to.equal(payload.description);
    });
  });

  describe('DELETE /product/{product_id}', () => {
    const payload = {
      name: 'Update Product Name',
      sku: `Update Product SKU ${Date.now()}`,
      image: 'Update Product Image',
      description: 'Update Product Description',
      price: 10000,
    };

    it('response with unauthorization', async () => {
      const pId = products[0].id;
      const res = await server.inject({
        method: 'delete',
        url: `/product/${pId}`,
        payload,
      });
      expect(res.statusCode).to.be.equal(401);
    });

    it('response with product not found', async () => {
      const res = await server.inject({
        method: 'delete',
        url: `/product/c47bf9ec-f3f9-456d-8b21-766f2fda13e1`,
        headers: {
          Authorization: 'Bearer ' + process.env.API_SECRET_KEY,
        },
      });

      expect(res.statusCode).to.be.equal(404);
    });

    it('response with product deleted', async () => {
      const pId = products[0].id;
      const res = await server.inject({
        method: 'delete',
        url: `/product/${pId}`,
        payload,
        headers: {
          Authorization: 'Bearer ' + process.env.API_SECRET_KEY,
        },
      });
      expect(res.statusCode).to.be.equal(200);
    });
  });
});
