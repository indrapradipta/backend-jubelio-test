'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = (exports.lab = Lab.script());
const { init } = require('../lib');

describe('GET /', () => {
  let server;

  beforeEach(async () => {
    server = await init();
  });

  afterEach(async () => {
    await server.stop();
  });

  it('responds with payload Hello World!', async () => {
    const res = await server.inject({
      method: 'get',
      url: '/',
    });
    expect(res.payload).to.equal('Hello World!');
  });
});
