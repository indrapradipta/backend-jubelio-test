# Backend Simple E-Commersse

# Getting started

## Install Node

Install [Node.JS LTS version](https://nodejs.org/en/download/)

## To get the Node server running locally

- Clone this repo
- `cd /path/where/your/cloned/the/repo`
- `npm install` to install all required dependencies
- Install PostgreSQL ([instructions](https://www.postgresql.org/download/))
- Create your database on Postgresql
- Setup `.env` Variable

```PORT=7000
API_SECRET_KEY=your_apikey_secret
ELEVENIA_OPEN_API=your_elevenia_openapi_key
DB_HOST=localhost
DB_PORT=5432
DB_NAME=your_database_name
DB_USER=your_database_username
DB_PASSWORD=your_database_password
```

- Run database migration `dbmigration:run`
- Start server localy `npm start`

## Dependencies

- [hapijs](https://github.com/hapijs/hapi) - The server for handling and routing HTTP requests
- [axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js
- [pg-promise](https://github.com/vitaly-t/pg-promise) - For handling database query

# Author

### [Indra Pradipta](https://github.com/iprdpta)
